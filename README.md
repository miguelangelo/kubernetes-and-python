# Kubernetes + Python = ❤

While Go is the language-of-choice in the cloud-native world, Python has a huge community and makes it really easy to extend Kubernetes in only a few lines of code.
This repository shows examples on how to use Python to query the Kubernetes API, how to write simple controllers in only 10 lines of Python, how to build complete web UIs, and how to test everything with py.test and Kind.
See the [Cloud Native Prague meetup talk on 2019-09-05](https://www.meetup.com/Cloud-Native-Prague/events/263802447/) and [the slides](https://www.slideshare.net/try_except_/kubernetes-python-cloud-native-prague/).

## Prerequisites

* Python 3.7
* [Poetry](https://poetry.eustace.io/)

## Running Python on Kubernetes

## Accessing Kubernetes from Python

## Writing Simple Controllers with Python

## Writing Operators with Python

