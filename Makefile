default: build

.PHONY: format
format:
	black -t py37 .

.PHONY: lint
lint:
	flake8

.PHONY: build
build: format lint
	poetry run ./build.py
