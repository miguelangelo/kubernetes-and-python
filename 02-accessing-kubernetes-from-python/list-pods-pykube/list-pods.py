#!/usr/bin/env python3
"""
Basic example to list pods and their container images.

See also https://pykube.readthedocs.io/
"""

import argparse

from pykube import HTTPClient, KubeConfig, Pod

parser = argparse.ArgumentParser()
parser.add_argument("namespace")
args = parser.parse_args()

api = HTTPClient(KubeConfig.from_file())

for pod in Pod.objects(api).filter(namespace=args.namespace):
    images = [c["image"] for c in pod.obj["spec"]["containers"]]
    print(pod.name, ", ".join(images))
