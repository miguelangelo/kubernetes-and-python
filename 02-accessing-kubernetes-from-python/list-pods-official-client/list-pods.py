#!/usr/bin/env python3
"""
Basic example to list pods and their container images.

See also https://github.com/kubernetes-client/python
"""

import argparse

from kubernetes import client, config

parser = argparse.ArgumentParser()
parser.add_argument("namespace")
args = parser.parse_args()

config.load_kube_config()
v1 = client.CoreV1Api()
ret = v1.list_namespaced_pod(args.namespace)
for pod in ret.items:
    images = [c.image for c in pod.spec.containers]
    print(pod.metadata.name, ", ".join(images))
