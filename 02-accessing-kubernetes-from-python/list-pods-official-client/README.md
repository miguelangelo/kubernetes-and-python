# list-pods.py

Basic example to list Pods on the command line using the [official Kubernetes Python client](https://github.com/kubernetes-client/python)

```
poetry run ./list-pods.py mynamespace
```
