"""
Simplified example script to set replicas to zero, no error handling!

See https://github.com/hjacobs/kube-downscaler for a production-ready controller to downscale
"""

import datetime, os, pykube, time

while True:
    # loads in-cluster auth or local ~/.kube/config for testing
    config = pykube.KubeConfig.from_env()
    api = pykube.HTTPClient(config)

    # no timezone handling!
    weekday = datetime.datetime.today().isoweekday()
    is_weekend = weekday in (6, 7) or os.getenv("ALWAYS_WEEKEND")

    for deploy in pykube.Deployment.objects(api, namespace=pykube.all):
        if "scale-down-on-weekend" in deploy.annotations and is_weekend:
            print(f"Updating deployment {deploy.namespace}/{deploy.name}..")
            deploy.replicas = 0
            deploy.update()
    time.sleep(15)
